import { Outlet, useNavigate } from "react-router-dom";
//
const ContentLayout = () => {
  const navigate = useNavigate();
  return (
    <div>
      <div>
        <button onClick={() => navigate("/")}>Home</button>
        <button onClick={() => navigate("/todo")}>Todo</button>
        <button onClick={() => navigate("/about")}>About</button>
      </div>
      <hr />

      <Outlet />
    </div>
  );
};

export default ContentLayout;
