import React from "react";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
//
import ContentLayout from "./layouts/ContentLayout";
//
import Home from "./pages/Home";
import About from "./pages/About";
import TodoList from "./pages/todo/TodoList";
import TodoForm from "./pages/todo/TodoForm";
import TodoFormUpdate from "./pages/todo/TodoFormUpdate";
//
function App() {
  const router = createBrowserRouter([
    {
      path: "/",
      element: <ContentLayout></ContentLayout>,
      children: [
        {
          path: "/",
          element: <Home />,
        },
        {
          path: "/about",
          element: <About />,
        },
        {
          path: "/todo",
          element: <TodoList />,
        },
        {
          path: "/todo-form",
          element: <TodoForm />,
        },
        {
          path: "/todo-form/:id",
          element: <TodoFormUpdate />,
        },
      ],
    },
  ]);
  return (
    <React.Fragment>
      <RouterProvider router={router} />
    </React.Fragment>
  );
}

export default App;
