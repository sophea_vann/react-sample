import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";

const TodoFormUpdate = () => {
  const navigate = useNavigate();
  const { id } = useParams();
  // hook useState
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  //
  const [todoList, setTodoList] = useState(
    JSON.parse(localStorage.getItem("todoList")) || []
  );

  //
  useEffect(() => {
    if (!todoList[id]) {
      return navigate("/todo");
    }
    setTitle(todoList[id].title);
    setDescription(todoList[id].description);
  }, []);

  //
  const onUpdateTodo = () => {
    // console.log("update");
    const doc = { title, description };
    console.log(doc);
    //
    const arr = todoList;
    arr[id] = doc;
    // console.log("arr", arr);
    localStorage.setItem("todoList", JSON.stringify(arr));
    //
    navigate("/todo");
  };

  //

  return (
    <div>
      <h1>Todo Form Update</h1>
      <input
        type="text"
        value={title}
        onChange={(e) => setTitle(e.target.value)}
        placeholder="Title"
      />
      <br />
      <br />
      <input
        type="text"
        value={description}
        onChange={(e) => setDescription(e.target.value)}
        placeholder="Description"
      />
      <br />
      <br />
      <button onClick={onUpdateTodo}>Update</button>
    </div>
  );
};

export default TodoFormUpdate;
