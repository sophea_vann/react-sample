import { useState } from "react";
import { useNavigate } from "react-router-dom";

const TodoForm = () => {
  const navigate = useNavigate();
  // hook useState
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  //

  //
  const onCreateTodo = () => {
    if (!title) return alert("Please enter title");
    const doc = {
      title,
      description,
    };
    console.log("doc", doc);
    const arr = JSON.parse(localStorage.getItem("todoList")) || [];
    arr.push(doc);
    localStorage.setItem("todoList", JSON.stringify(arr));

    //
    navigate("/todo");
  };
  //

  return (
    <div>
      <h1>Todo Form</h1>
      <input
        type="text"
        value={title}
        onChange={(e) => setTitle(e.target.value)}
        placeholder="Title"
      />
      <br />
      <br />
      <input
        type="text"
        value={description}
        onChange={(e) => setDescription(e.target.value)}
        placeholder="Description"
      />
      <br />
      <br />
      <button onClick={onCreateTodo}>Create</button>
    </div>
  );
};

export default TodoForm;
