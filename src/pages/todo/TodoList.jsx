import { useState } from "react";
import { useNavigate } from "react-router-dom";

const TodoList = () => {
  const navigate = useNavigate();
  //
  const [todoList, setTodoList] = useState(
    JSON.parse(localStorage.getItem("todoList")) || []
  );
  //
  return (
    <div>
      <h1>Todo List</h1>
      <button onClick={() => navigate("/todo-form")}>Create Todo</button>
      <table border={1} width="100%">
        <thead>
          <tr>
            <th>No</th>
            <th>Title</th>
            <th>Description</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          {todoList.map((todo, index) => (
            <tr key={index}>
              <td>{index + 1}</td>
              <td>{todo.title}</td>
              <td>{todo.description}</td>
              <td>
                <button onClick={() => navigate(`/todo-form/${index}`)}>
                  Edit
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default TodoList;
